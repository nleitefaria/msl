insert into product (id, name, creation_date) values (1, 'Carbonated Water - Blackcherry', '2023-10-17 12:12:24');
insert into product (id, name, creation_date) values (2, 'Dill Weed - Dry', '2023-08-02 12:12:24');
insert into product (id, name, creation_date) values (3, 'Waffle Stix', '2023-01-29 12:12:24');
insert into product (id, name, creation_date) values (4, 'Beef - Prime Rib Aaa', '2023-05-04 12:12:24');
insert into product (id, name, creation_date) values (5, 'Worcestershire Sauce', '2023-05-16 12:12:24');
