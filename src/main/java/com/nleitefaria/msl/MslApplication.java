package com.nleitefaria.msl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MslApplication {

	public static void main(String[] args) {
		SpringApplication.run(MslApplication.class, args);
	}

}
