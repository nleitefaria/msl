package com.nleitefaria.msl.init;

import com.nleitefaria.msl.dto.ProductDTO;
import com.nleitefaria.msl.entity.Product;
import com.nleitefaria.msl.mapper.ProductMapper;
import com.nleitefaria.msl.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class InitDB {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    @Bean
    public CommandLineRunner testProductMapper() {

        return args -> {

            List<Product> productList = productRepository.findAll();
            productList.forEach(System.out::println);

            List<ProductDTO> productDTOList = productList.stream().map(product -> productMapper.productToProductDTO(product))
                    .peek(System.out::println)
                    .collect(Collectors.toList());

            List<ProductDTO> productDTOList1 = productMapper.toProductDTOList(productList);
            productDTOList.forEach(System.out::println);

            List<Product> productList1 = productMapper.toProductList(productDTOList);
            productList1.forEach(System.out::println);
        };
    }
}
