package com.nleitefaria.msl.mapper;

import com.nleitefaria.msl.dto.ProductDTO;
import com.nleitefaria.msl.entity.Product;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProductMapper {

    @Mappings({
            @Mapping(source = "creationDdate", target = "creationDdate", dateFormat = "yyyy-MM-dd HH:mm:ss")
        }
    )
    ProductDTO productToProductDTO(Product product);

    @InheritInverseConfiguration
    Product productDTOToProduct(ProductDTO productDTO);

    List<ProductDTO> toProductDTOList(List<Product> productList);

    List<Product> toProductList(List<ProductDTO> productDTOList);

}
